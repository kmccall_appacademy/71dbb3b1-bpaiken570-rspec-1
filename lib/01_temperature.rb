#01_temperature.rb

def ftoc(far_temp)
  ((far_temp - 32.0) * 5.0 / 9.0).round(1)
end

def ctof(cel_temp)
  (cel_temp * 9.0 / 5.0 + 32.0).round(1)
end
