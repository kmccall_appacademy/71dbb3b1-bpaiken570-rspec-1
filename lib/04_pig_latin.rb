# 04_pig_latin.rb

def translate(sentence)
  sentence.split.map { |word| pig_word(word) }.join(' ')
end

def pig_word(word)
  arr = word.chars
  v_index = word.index(/[aioue]/)

  if arr[v_index - 1] == 'q' # check for 'qu' condition
    arr = arr.rotate(v_index + 1)
  elsif v_index
    arr = arr.rotate(v_index)
  end
  arr.join + 'ay'
end
