# 03_simon_says.rb

def echo(string)
  string
end

def shout(string)
  string.upcase
end

def repeat(string, num_repeat = 2)
  ("#{string} " * num_repeat).rstrip
end

def start_of_word(string, num_chars)
  string[0...num_chars]
end

def first_word(string)
  string.split.first
end

def titleize(string)
  little_words = %w[a an and the by for in of on as over]
  title = string.downcase.split
  result = title.each_with_index.map do |word, idx|
    word.capitalize! if !little_words.include?(word) || idx == 0
    word
  end
  result.join(' ')
end
