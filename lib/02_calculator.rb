# 02_calculator.rb

def add(num1, num2)
  num1 + num2
end

def subtract(num1, num2)
  num1 - num2
end

def sum(num_array)
  if num_array.empty?
    0
  else
    num_array.reduce(:+)
  end
end
